const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
var dbFlie = require('./data/user');

const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.get('/', (req, res) => res.send('api spc'));
app.get('/api/customers',  (req, res) => res.send(dbFlie.Customers));


app.post('/api/customers', function(req, res) { 
    console.log('add new customer ' +  JSON.stringify(req.body)) 
    res.send(JSON.stringify(req.body));
});

app.delete('/api/customers/:id', function(req, res) { 
    console.log('delete customer ' + req.params.id) 
    res.send({msg: 'success'});
});

app.get('/api/customers/:id', function (req, res)  {
    var  itemId = req.params.id;
    console.log(itemId);
    var item = dbFlie.Customers.filter(c=>c.CustomerID == itemId);
    console.log(item);
    if (item) {
       res.json(item[0]);
    } else {
       res.json({ message: `item ${itemId} doesn't exist`})
    }
 });

app.listen(port, () => console.log(`Example app listening on port ${port}!`))